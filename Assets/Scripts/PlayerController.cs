using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocityX = 15;
    public float jumpForce = 250;

    // private components
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    
    
    // private properties
    private bool life = true;
    
    // constants
    private const int ANIMATION_RUN = 0;
    private const int ANIMATION_DEAD = 1;
    private const int ANIMATION_JUMP = 2;

    private const int LAYER_GROUND = 10;

    private const string TAG_ENEMY = "Enemy";
    
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
        cambiaAnimation(ANIMATION_RUN); 
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y); 
            sr.flipX = false;
            cambiaAnimation(ANIMATION_RUN);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // salta
            Debug.Log("Saltando");
            cambiaAnimation(ANIMATION_JUMP); // saltar
        }
        if(!life){
            cambiaAnimation(ANIMATION_DEAD);            
            velocityX = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(TAG_ENEMY))
        {
            life=false;
            cambiaAnimation(ANIMATION_DEAD);
            //Destroy(gameObject);
            // Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>(), true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
       Debug.Log("Trigger:" + this.name);
    }


    private void cambiaAnimation(int animation)
    {
        Debug.Log("Cambia Animacion");
        animator.SetInteger("estado", animation);
    }
}
