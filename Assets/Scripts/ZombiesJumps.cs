using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesJumps : MonoBehaviour
{
        // private components
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;

    public float jumpForce = 250;
    //privados
    private const string TAG_SUELO = "Suelo";
    bool salto = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!salto)
            saltar();
    }
    void saltar(){
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        salto = true;  
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(TAG_SUELO))
            salto=false;
    }
}
