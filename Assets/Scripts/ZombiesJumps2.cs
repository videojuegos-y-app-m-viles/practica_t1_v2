using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesJumps2 : MonoBehaviour
{
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;

    public float velocityX = -10;
    public float jumpForce = 250;
    //privados
    private const string TAG_SUELO = "Suelo";
    bool salto = false;

    bool gira = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
     void Update()
    {
        if(!salto)
            saltar();
    }
    void saltar(){
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        //rb.velocity = new Vector2(velocityX, rb.velocity.y); 
        salto = true;
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(TAG_SUELO))
        {
            salto=false; 
            if(!gira) 
            {
                sr.flipX = true;
                gira=true;
            }
            else
            {
                 sr.flipX = false;
                gira=true;
            }
            
            //rv.velocityX *=-1; 7
        }
    }

}
